# Create an Instance
resource "aws_instance" "sample-Instance" {
  ami = "ami-0648ea225c13e0729" # AWS software image on eu-west-2 London AZ
  instance_type = "t2.micro" #Virtual machine type
  user_data = file("${path.module}/app1-install.sh")
  tags = {
    "Name" = "EC2 Demo"
  }

}