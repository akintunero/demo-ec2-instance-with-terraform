# Terraform settings block
terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.39.0" 
    }
  }
}

# Terraform provider block
provider "aws" {
  region = "eu-west-2"

  

}

 